package com.bbva.uuaa.lib.r001.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.apx.exception.db.DuplicateKeyException;
import com.bbva.uuaa.dto.banco.CuentaDTO;
import com.bbva.uuaa.dto.banco.PaginationInDTO;
import com.bbva.uuaa.dto.banco.PaginationOutDTO;

/**
 * The  interface UUAAR001Impl class...
 */
public class UUAAR001Impl extends UUAAR001Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(UUAAR001Impl.class);

	/**
	 * The execute method...
	 */
	
	//METODOS EXECUTE DECLARADOS EN UUAAR001
	//METODO QUE ACTUALIZA LA BASE 
	@Override
	public boolean executeCreate (CuentaDTO account, CuentaDTO divisa,
			CuentaDTO tipoCuenta, CuentaDTO importe ){ //DATOS PROVENIENTES DEL DTO. SI FUERA BATCH NO IRIA DTO SI NO LA REFERNCIA AL ARCHIVO TXT,CSV,EXCEL...
		
		LOGGER.info("inicia el insert de datos");
		
		//HACEMOS REFERENCIA A LOS DATOS DE LA TABLA NOMBRADOS TAL CUAL EN EL PROPERTIES,
		//LOS AGREGAMOS A UN MAP PARA PODER TENER LA LLAVE VALOR 
		//y tambien hacemos referencia a los datos del DTO
		Map<String,Object>args=new HashMap<>();
		
		args.put("DIVISA",divisa.getCdDivisa()); 
		args.put("CUENTA",account.getNumCuenta());
		args.put("TCUENTA",tipoCuenta.getCdTipoCuenta());
		args.put("IMPORTE",importe.getImporte());
		
		//CACHAMOS EL ERROR DE CONEXION Y EJECUCION HACIA LA BASE PARA PERMITIR EL SEGUIMIENTO DEL DESARROLLO
		try {
			
		
		jdbcUtils.update("INSER_CUENTA",args);
		
		
		}catch(DuplicateKeyException ex){
			this.addAdvice("UUAA000122");
		}
		this.addAdvice("UUAA0002");
		return false;
	}
	
	
	//METODO QUE TRAERA EL SELECT (CONSULTA) DE LA BASE DE DATOS
	//ES NECESARIO ESCRIBIR EXECUTE EN CADA METODO
	@Override 
		public List<CuentaDTO> executeRead(Double cuenta, PaginationInDTO paginationIn){
		LOGGER.info("inicia la lectura de datos");
		Map<String,Object>args=new HashMap<>();
		args.put("CUENTA",cuenta);
		
		int firstRow=paginationIn.getPaginationkey()==1? paginationIn.getPaginationkey(): paginationIn.getPaginationSize()*(paginationIn.getPaginationkey()-1)+1;
		List<Map<String,Object>> cuentas = jdbcUtils.pagingQueryForList("SELECT_CUENTA",firstRow,paginationIn.getPaginationSize());
        List<CuentaDTO> listCuentas=new ArrayList<>();
        
       
        
		for(Map<String,Object>row:cuentas){
			LOGGER.info("{}",row);
		
			CuentaDTO rowDTO= new CuentaDTO();
			rowDTO.setCdDivisa(row.get("CDDIVISA").toString());
			rowDTO.setCdTipoCuenta((row.get("CDTIPOCUENTA").toString()));
			rowDTO.setImporte(Long.parseLong(row.get("IMPORTE").toString()));
			rowDTO.setImporte(Long.parseLong(row.get("NUCUENTA").toString()));
			
			listCuentas.add(rowDTO);
		
		}
		
		return  listCuentas;
	}
		
		@Override
		
		public PaginationOutDTO getPaginationOut(Double cuenta, PaginationInDTO paginationIn){
			PaginationOutDTO paginationOut= new PaginationOutDTO();
			Map<String,Object>args= new HashMap<>();
			args.put("CUENTA",cuenta.longValue());
			int total = this.jdbcUtils.queryForInt("SELECT_CUENTA", args);
			paginationOut.setHasMoreData((total>(paginationIn.getPaginationkey()*paginationIn.getPaginationSize()))?true:false);
			paginationOut.setPaginationKey((total>(paginationIn.getPaginationkey()*paginationIn.getPaginationSize()))? paginationIn.getPaginationkey()+1:paginationIn.getPaginationSize());
			return paginationOut;
		}
}
